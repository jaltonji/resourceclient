VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "WSIDBUtil"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Function ReplaceNull(value As Variant) As String
    If IsNull(value) Then
        ReplaceNull = ""
    Else
        ReplaceNull = value
    End If
End Function
Public Function ReplaceEmptyVString(value As Variant) As Variant
    If value = "" Then
        ReplaceEmptyVString = Null
    Else
        ReplaceEmptyVString = value
    End If
End Function

Public Function ReplaceEmptyString(value As String) As Variant
    If value = "" Then
        ReplaceEmptyString = Null
    Else
        ReplaceEmptyString = value
    End If
End Function

Public Function ReplaceNullNumeric(value As Variant) As Double
    If IsNull(value) Then
        ReplaceNullNumeric = 0
    Else
        ReplaceNullNumeric = value
    End If
End Function
Public Function ReplaceNullORBlankNumeric(value As Variant) As Double
    If IsNull(value) Or value = "" Then
        ReplaceNullORBlankNumeric = 0
    Else
        ReplaceNullORBlankNumeric = value
    End If
End Function

Public Function ReplaceNullOREmptyORBlankNumeric(value As Variant) As Double
    If IsNull(value) Or IsEmpty(value) Or value = "" Then
        ReplaceNullOREmptyORBlankNumeric = 0
    Else
        ReplaceNullOREmptyORBlankNumeric = value
    End If
End Function
Public Function ReplaceZero(value As Variant) As Variant
    If value = 0 Then
        ReplaceZero = Null
    Else
        ReplaceZero = value
    End If
End Function

Public Function FormatSSN(SSN As String) As String
    FormatSSN = Left(SSN, 3) + "-" + Mid(SSN, 4, 2) + "-" + Right(SSN, 4)
End Function
Public Function SecureSSN(SSN As String) As String
    Dim nLen As Integer
    Dim str As String
    
    str = SSN ' FormatSSN(udtprops.SSN)
    On Error Resume Next
    nLen = Len(str)
    If (nLen >= 4) Then
       str = Mid$(str, nLen - 3, 4)
    'Else
    '   str = Me.SSN
    End If
    
    SecureSSN = str

End Function

Public Function CleanSSN(SSN As String) As String
    
    Dim pos As Long
    
    pos = InStr(1, SSN, "-")
    If pos = 0 Then
        CleanSSN = SSN
    Else
        CleanSSN = Mid(SSN, 1, pos - 1) + CleanSSN(Mid(SSN, pos + 1))
    End If
    
End Function


