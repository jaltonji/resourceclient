VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "WSIDB"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Date:          07/09/98

'Company        WebStor Inc

'Coder:         Matthew Clayton

'Description:   Connection Object to Mange ADO Connections. There will be a global connection
'               object used with this project. The connection will be made on startup of the program.
'               eventually this will become a Server that all dlls will use

' 07/07/98      Created
' 08/14/98      Added Function to Return Next ID number given Counter Name
' 02/12/99      Added Transaction function. Users can either use these are
'               call the ado function directly
' 03/22/99      Made changes to class and wrapped it in it's own DLL so that it
'               can be used by multiple projects
' 03/22/99      Added Data Source Property and Logon method
' 09/29/99      Added DateTime Property
' 09/29/99      Added TimeStampsEqual Function
'
' 12/07/07      Added If (InStr(1, mvarDataSource, "OLEDB ") = 1) Then to support non-ODBC conn string
'
' 02/28/22      Added db.SmallDateTime method to support patientappointments.DateScheduled field which is smalldatetime
'
'



Option Explicit

Public Enum wsiDBGroups
    grpAdmin = -1
    grpMD = 1
    grpNursing = 2
    grpEmb = 3
    grpPhlebotomist = 4
    grpCA = 5
    grpLabOrder = 6
    grpLabResult = 7
    grpAndrology = 8
    grpSurgAssist = 9
    grpPatServ = 10
    grpFinance = 11
    grpDemoAdmin = 12
    grpIVFSched = 13
    grpProgNoteEditor = 14
    grpAdminEmbLab = 15
    grpEmbRetLtrPrn = 16
    grpAdminSart = 17
    grpAdminOffice = 18
    grpAdminLab = 19
    grpPrescribers = 28
End Enum

'-----------------Private Variables--------------------------------------------
Private mvarConn As ADODB.Connection
Private mvarUser As String
Private mvarPasswd As String
Private mvarDataSource As String
Private mvarUserID As Long
Private mvarPracticeID As Long
Private mvarTransLevel As Long
Private mvarUserGroupId As Long

Private flgLogin As Boolean
'Private objLog As Log
Private nHelpId As Long
Private Enum TimeZones
   eastern = 0
   central = 1
   mountain = 2
   pacific = 3
End Enum

Private Enum ColorModes
   minColor = 0
   maxColor = 1
End Enum
Private Const ResetPassword = "109876054321RES9our3ce"

Private m_timeZone As Integer
Private m_bMailSysMessages As Boolean
Private m_strMailSysMessagesRecipient As String
Private m_nColorMode As Long
Private m_OSAuthenticationMode As Boolean

Private m_strImageDB As String
Private m_strImageDBPat As String
Private m_bHaveImageDb As Boolean
Private m_bHaveImageDbPat As Boolean

Private m_strArchiveDB As String


Private m_bGroupsLoaded As Boolean
Private m_colGroups As Collection


Public Property Let OSAuthentication(ByVal val As Boolean)
   m_OSAuthenticationMode = val
End Property



'-----------------Private Methods--------------------------------------------

'Function Name:     Class Initialize
'Description:       Ma ke a connection to the Database when the class is initialize
Private Sub Class_Initialize()
    Set mvarConn = New ADODB.Connection
 
    m_nColorMode = ColorModes.maxColor
    m_timeZone = TimeZones.eastern
    m_bMailSysMessages = False
    m_OSAuthenticationMode = False
    m_strImageDB = ""
    m_strImageDBPat = ""
    m_bHaveImageDb = False
    m_bHaveImageDbPat = False
    
    m_strArchiveDB = ""
    
    'Set objLog = New Log
    'objLog.SetParent Me
    
    
End Sub

'Function Name:     Class Terminate
'Description:       Cleanup when the object goes out of scope

Private Sub Class_Terminate()
    If (mvarConn.State <> adStateClosed) Then
       mvarConn.Close
    End If
    
    Set mvarConn = Nothing
    ' objLog.WriteLog
    Set m_colGroups = Nothing
    
    
    
End Sub

'Function Name:     GetConnection
'Description:       Returns the ADO connection

Public Function GetConnection() As ADODB.Connection

    Dim strCnn As String

'Driver={SQL Server};" & _
'                   "Server=carl2;" & _
'                   "Database=pubs;" & _
'                   "Uid=sa;" & _
'                   "Pwd=;"


    If mvarConn.State <> adStateOpen Then
        If (InStr(1, mvarDataSource, "OLEDB ") = 1) Then
           strCnn = Mid$(mvarDataSource, 7)
        Else
           strCnn = "driver={SQL Server};Data Source=" + mvarDataSource
        End If
        
        'strCnn = "driver={SQL Server};Server=jaltonji1;Database=resource;QuotedId=No"
        If (m_OSAuthenticationMode = False) Then
           
           mvarConn.Open strCnn, mvarUser, mvarPasswd
           
           'If (mvarConn.Errors.Count > 0) Then
           '   MsgBox "Conn Error: " & mvarConn.Errors.Item(1)
           'End If
           
           'If ((mvarConn.State <> adStateOpen) And (mvarPasswd <> ResetPassword)) Then  ' assume auth mode was changed
           '   mvarConn.Open strCnn, "", ""  ' to support NT Security
           'End If
        Else
           mvarConn.Open strCnn, "", ""  ' to support NT Security
        End If
    End If
    
    
    Set GetConnection = mvarConn
    
End Function

'Supply the read/write properties for user
Public Property Let User(value As String)
    mvarUser = value
End Property
Public Property Get User() As String
    User = mvarUser
End Property
Public Property Let MailSysMessages(value As Boolean)
   m_bMailSysMessages = value
End Property
Public Property Get MailSysMessages() As Boolean
   MailSysMessages = m_bMailSysMessages
End Property
Public Property Let MailSysMessagesRecipient(value As String)
   m_strMailSysMessagesRecipient = value
End Property
Public Property Get MailSysMessagesRecipient() As String
   MailSysMessagesRecipient = m_strMailSysMessagesRecipient
End Property
Public Property Get CurrentUserTemplateGroupId() As Long

    Static flgUTIDRetrieved As Boolean
    Dim strSQL As String
    Dim rst As ADODB.Recordset
    
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
    
    If Not flgUTIDRetrieved Then
    
       Set rst = New ADODB.Recordset
       strSQL = "SELECT TemplateGroupId From UserTbl where username = '" & Me.User & "'"
       rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
        
       If rst.EOF Then
           Err.Raise 445
       End If
        
       mvarUserGroupId = rst!TemplateGroupId
       rst.Close
       Set rst = Nothing
       flgUTIDRetrieved = True
       
    End If
    
    CurrentUserTemplateGroupId = mvarUserGroupId
    
End Property
Public Property Get CurrentUser() As Long
    Static flgIDRetrieved As Boolean
    Dim strSQL As String
    Dim rst As ADODB.Recordset
    
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
    
    If Not flgIDRetrieved Then
        
        Set rst = New ADODB.Recordset
        strSQL = "SELECT UserID From UserTbl where username = '" & Me.User & "'"
        rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If rst.EOF Then
            Err.Raise 445
        End If
        
        mvarUserID = rst!Userid
        rst.Close
        Set rst = Nothing
        
        flgIDRetrieved = True

    End If
    
    CurrentUser = mvarUserID
    
End Property
Public Property Get UserPractice() As Long
    Static flgUPIDRetrieved As Boolean
    Dim strSQL As String
    Dim rst As ADODB.Recordset
    
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
    
    If Not flgUPIDRetrieved Then
        
        Set rst = New ADODB.Recordset
        strSQL = "SELECT PracticeID From UserTbl where username = '" & Me.User & "'"
        rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If rst.EOF Then
            Err.Raise 445
        End If
        
        mvarPracticeID = rst!PracticeId
        rst.Close
        Set rst = Nothing

        flgUPIDRetrieved = True
    End If
    
    UserPractice = mvarPracticeID
    
End Property


Public Function IsInGroup(grp As wsiDBGroups, Optional Userid As Long = -1) As Boolean
        
    
    If (Userid = -1) Then
    
       ' get current user's group permission
       If (m_bGroupsLoaded = False) Then
          LoadGroups
       End If
    
       Dim i As Integer
       For i = 1 To m_colGroups.Count
          If m_colGroups.Item(i) = grp Then
             IsInGroup = True
             Exit Function
          End If
       Next i
    
       IsInGroup = False
    
    Else
    
       Dim rst As ADODB.Recordset
       Dim strSQL As String
       'Dim UserIdentifier As Long
       
       'UserIdentifier = Userid
    
       If Me.GetConnection.State <> adStateOpen Then
          Err.Raise 383
       End If
        
       Set rst = New ADODB.Recordset
       strSQL = "SELECT UserID, GroupID From UserToGroup where UserID=" & Userid & " and GroupID =" & grp
       rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
       If rst.EOF Then
          IsInGroup = False
       Else
          IsInGroup = True
       End If
   
       rst.Close
       Set rst = Nothing
    
    End If
    
        
End Function
Private Sub LoadGroups()
        
    Dim rst As ADODB.Recordset
    Dim strSQL As String
    'Dim UserIdentifier As Long
    
    'UserIdentifier = Me.CurrentUser
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
        
    Set rst = New ADODB.Recordset
    strSQL = "SELECT GroupID From UserToGroup where UserID=" & Me.CurrentUser
    rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set m_colGroups = New Collection
    Dim obj As Integer
    
    While Not rst.EOF
       obj = rst!GroupID
       m_colGroups.Add obj
       rst.MoveNext
    Wend
    
    
    rst.Close
    Set rst = Nothing
    
    m_bGroupsLoaded = True
        
End Sub

Public Function ConfirmPassword(Password As String) As Boolean
    'If StrComp(Password, mvarPasswd, vbBinaryCompare) = 0 Then
    If StrComp(Password, mvarPasswd, vbTextCompare) = 0 Then
        ConfirmPassword = True
    Else
        ConfirmPassword = False
    End If
End Function

'The Password property is Write only
Public Property Let Password(value As String)
    mvarPasswd = value
End Property
Public Property Let DataSource(value As String)
    mvarDataSource = value
End Property

Public Property Get DataSource() As String
    DataSource = mvarDataSource
End Property

'Function Name:     GetNextID
'Description:       Returns The Next ID for the given counter and updates to that
'                   count value.

Public Function GetNextID(CountName As String) As Long

    Dim prmRetVal As New ADODB.Parameter
    Dim prmCountName As New ADODB.Parameter
    Dim cmd As New ADODB.Command
 

    prmRetVal.Type = adInteger
    prmRetVal.Direction = adParamReturnValue
    cmd.Parameters.Append prmRetVal

    prmCountName.Type = adVarChar
    prmCountName.Size = Len(CountName)
    prmCountName.Direction = adParamInput
    prmCountName.value = CountName
    cmd.Parameters.Append prmCountName

    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "sp_SQLIncCount"
    Set cmd.ActiveConnection = GetConnection
    cmd.Execute
    Set cmd.ActiveConnection = Nothing

    GetNextID = cmd.Parameters(0).value

End Function

'Function Name:     GetNextID
'Description:       Returns The Next ID for the given counter and updates to that
'                   count value.

Public Function GetNextID_1(CountName As String, ByVal strTblId As String) As Long

    Dim prmRetVal As New ADODB.Parameter
    Dim prmCountName As New ADODB.Parameter
    Dim cmd As New ADODB.Command
 

    prmRetVal.Type = adInteger
    prmRetVal.Direction = adParamReturnValue
    cmd.Parameters.Append prmRetVal

    prmCountName.Type = adVarChar
    prmCountName.Size = Len(CountName)
    prmCountName.Direction = adParamInput
    prmCountName.value = CountName
    cmd.Parameters.Append prmCountName

    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "sp_SQLIncCount" & "_" & strTblId
    Set cmd.ActiveConnection = GetConnection
    cmd.Execute
    Set cmd.ActiveConnection = Nothing

    GetNextID_1 = cmd.Parameters(0).value

End Function

' Just retrieve what the next count value would be.
' Does not update the cvount
Public Function SeeNextID(CountName As String) As Long

    Dim prmRetVal As New ADODB.Parameter
    Dim prmCountName As New ADODB.Parameter
    Dim cmd As New ADODB.Command
 

    prmRetVal.Type = adInteger
    prmRetVal.Direction = adParamReturnValue
    cmd.Parameters.Append prmRetVal

    prmCountName.Type = adVarChar
    prmCountName.Size = 20
    prmCountName.Direction = adParamInput
    prmCountName.value = CountName
    cmd.Parameters.Append prmCountName

    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "sp_SQLGetCount"
    Set cmd.ActiveConnection = GetConnection
    cmd.Execute
    Set cmd.ActiveConnection = Nothing

    SeeNextID = cmd.Parameters(0).value

End Function

Public Function Logon(UserName As String, Password As String, DataSource As String) As Boolean
    
    Dim objConnection As ADODB.Connection

    Me.User = UserName
    Me.Password = Password
    
    Me.DataSource = mvarDataSource
    If Len(Me.DataSource) = 0 Then
       Me.DataSource = DataSource
    End If
    
    On Error GoTo ERR_Connection
    
    Set objConnection = Me.GetConnection

    If objConnection.State = adStateOpen Then
        Logon = True
        flgLogin = True
        GetTimeZone
    Else
        Logon = False
        flgLogin = False
    End If
    
    Exit Function
    
ERR_Connection:
    Logon = False
    flgLogin = False
    
    
    'MsgBox Err.Description, vbInformation
    
    
    
End Function

Public Sub SetAppRole(RoleName As String, Password As String)
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
    
    Dim strSetAppRole As String
    
    strSetAppRole = "Exec sp_setapprole '" & RoleName & "', {Encrypt N '" & Password & "'}, 'odbc'"
    
    Me.GetConnection.Execute strSetAppRole
    
    
    
End Sub


Public Property Get TransLevel() As Long
    TransLevel = mvarTransLevel
End Property


Public Sub BeginTrans()
    If mvarConn.State <> adStateOpen Then
       GetConnection
    End If

    mvarTransLevel = mvarConn.BeginTrans
End Sub

Public Sub CommitTrans()
    mvarConn.CommitTrans
    mvarTransLevel = mvarTransLevel - 1
End Sub

Public Sub RollbackTrans()

    On Error GoTo ERR_Roll
    
    mvarConn.RollbackTrans
    mvarTransLevel = mvarTransLevel - 1
    
    Exit Sub
    
ERR_Roll:

    'MsgBox Err.HelpContext, vbInformation, Err.Number
    
    If (Err.Number = -2147418113) Then  ' db was disconnected.
        Set mvarConn = New ADODB.Connection
        mvarTransLevel = mvarTransLevel - 1
    End If
    
    Err.Raise Err.Number, Err.Source, Err.Description & " !!! Please check your last transaction to be sure it was Saved."
    
    
End Sub
Public Sub ResetConnection()
   Set mvarConn = New ADODB.Connection
End Sub
Public Function TimeStampsEqual(ts1 As Variant, ts2 As Variant) As Boolean
    
    Dim i As Integer

    For i = 7 To 0 Step -1
        If ts1(i) <> ts2(i) Then
            TimeStampsEqual = False
            Exit Function
        End If
    Next i
    
    TimeStampsEqual = True
    
End Function


'Public Property Get Log() As Log
    'If Not flgLogin Then Err.Raise 445
'    Set Log = Nothing 'objLog
'End Property

Public Property Get DateTime() As Variant
    
    Dim strSQL As String
    Dim rst As ADODB.Recordset
    
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
    
    ' get the timezone and subtract accordingly
    
        
    Set rst = New ADODB.Recordset
    'rst.CursorLocation = adUseClient
    'rst.CacheSize = 30
    'rst.CursorType = adOpenForwardOnly
    'rst.LockType = adLockReadOnly
    
    
    If m_timeZone = TimeZones.central Then
       strSQL = "SELECT GetDate() - .0416 as DateTime"
    ElseIf m_timeZone = TimeZones.mountain Then
       strSQL = "SELECT GetDate() - .083 as DateTime"
    ElseIf m_timeZone = TimeZones.pacific Then
       strSQL = "SELECT GetDate() - .125 as DateTime"
    Else
       strSQL = "SELECT GetDate() as DateTime"
    End If
         
    rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
   ' rst.Open strSQL, Me.GetConnection, adOpenStatic, adLockReadOnly, adCmdText
        
    DateTime = rst!DateTime
    rst.Close
    Set rst = Nothing
    
End Property

Public Property Get SmallDateTime() As Variant
    
    Dim strSQL As String
    Dim rst As ADODB.Recordset
    
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
    
    ' get the timezone and subtract accordingly
    
        
    Set rst = New ADODB.Recordset
    'rst.CursorLocation = adUseClient
    'rst.CacheSize = 30
    'rst.CursorType = adOpenForwardOnly
    'rst.LockType = adLockReadOnly
    
    
    If m_timeZone = TimeZones.central Then
       strSQL = "SELECT CAST(GetDate() - .0416 as smalldatetime) as DateTime"
    ElseIf m_timeZone = TimeZones.mountain Then
       strSQL = "SELECT CAST(GetDate() - .083 as smalldatetime) as DateTime"
    ElseIf m_timeZone = TimeZones.pacific Then
       strSQL = "SELECT CAST(GetDate() - .125 as smalldatetime) as DateTime"
    Else
       strSQL = "SELECT CAST(GetDate() as smalldatetime) as DateTime"
    End If
         
    rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
   ' rst.Open strSQL, Me.GetConnection, adOpenStatic, adLockReadOnly, adCmdText
        
    SmallDateTime = rst!DateTime
    rst.Close
    Set rst = Nothing
    
End Property
Public Property Get CurrentTime() As Variant
    
    Dim strSQL As String
    Dim DateTime As Variant
    Dim rst As ADODB.Recordset
    
    
    If Me.GetConnection.State <> adStateOpen Then
        Err.Raise 383
    End If
    
    DateTime = Me.DateTime
        
    Set rst = New ADODB.Recordset
    
    strSQL = "SELECT DATEPART(hh,'" & DateTime & "') as theHour, DATEPART(mi,'" & DateTime & "') as theMinute"
    rst.Open strSQL, Me.GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
    CurrentTime = rst!theHour & ":" & rst!theMinute
    rst.Close
    
    Set rst = Nothing
    
End Property

Public Function IsConnected() As Boolean
    'If Me.GetConnection.State <> adStateOpen Then
    If mvarConn.State <> adStateOpen Then
        IsConnected = False
    Else
        IsConnected = True
    End If
End Function
Public Function GetMessage(ByVal nMsgId As Long) As String
    
    Dim strSQL As String
    Dim rst As New ADODB.Recordset
    
    On Error GoTo ERR_Trap
    
    strSQL = "SELECT Description, HelpId, Enable From lkupMsgTbl where MsgId=" & nMsgId
             
    rst.Open strSQL, GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If rst.EOF Then
        GetMessage = "Unknown Message"
        rst.Close
        Set rst = Nothing
        Exit Function
    End If
        
    ' 080519 - JA Added enable check
    If (rst!Enable = True) Then
       GetMessage = rst!Description
       nHelpId = rst!HelpId
    Else
       GetMessage = ""
       nHelpId = 0
    End If
    
    rst.Close
    Set rst = Nothing
    
    Exit Function
    
ERR_Trap:

    GetMessage = "Unknown Message"

End Function
Public Property Get GetHelpId() As Long
    GetHelpId = nHelpId
End Property

'Public Property Get HelpId() As Long
'    HelpId = nHelpId
'End Property
Private Sub GetTimeZone()
    
    Dim strSQL As String
    Dim rst As New ADODB.Recordset
    
    On Error Resume Next   ' default to eastern (0)
    
    
    '
    ' 110106  May need to add TimeZone field to the Location record to support
    ' sites that have offices in multiple timezones.
    ' Or add to User Options, add a public interface to db to set the timeZone from the
    ' Logon object.
    '
    strSQL = "SELECT TimeZone From AppTimeZone"
    rst.Open strSQL, GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    m_timeZone = rst!TimeZone
    
    rst.Close
    Set rst = Nothing

End Sub

Public Property Get ColorMode() As Long
   ColorMode = m_nColorMode
End Property
Public Property Let ColorMode(ByVal nColorMode As Long)
   m_nColorMode = nColorMode
End Property
Public Function GetArchiveDB(ByVal nDBId As Integer) As String

    Dim strSQL As String
    Dim rst As New ADODB.Recordset
    
    On Error Resume Next
    
    If (m_strArchiveDB = "") Then
      
       strSQL = "SELECT EmailAddr from lkupSysTargets where TargetId = " & nDBId
       rst.Open strSQL, GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
      
       If (Not rst.EOF) Then
          m_strArchiveDB = rst!EmailAddr
       End If
      
       rst.Close

    End If
    
    GetArchiveDB = m_strArchiveDB
      
End Function
Public Function GetImageDB(ByVal nDBId As Integer) As String
   
   If ((m_bHaveImageDb = False And nDBId = 100) Or (m_bHaveImageDbPat = False And nDBId = 110)) Then
   
      Dim strSQL As String
      Dim rst As New ADODB.Recordset
    
      On Error Resume Next
    
      ' example of 100 entry database ref: 'RESourceImages.dbo.'
      If (nDBId = 100) Then
         m_bHaveImageDb = True
      ElseIf (nDBId = 110) Then
         m_bHaveImageDbPat = True
      End If
    
    
      'm_bHaveImageDb = True
      strSQL = "SELECT EmailAddr from lkupSysTargets where TargetId = " & nDBId
      rst.Open strSQL, GetConnection, adOpenForwardOnly, adLockReadOnly, adCmdText
      
      If (Not rst.EOF) Then
         If (nDBId = 100) Then
            m_strImageDB = rst!EmailAddr
         ElseIf (nDBId = 110) Then
            m_strImageDBPat = rst!EmailAddr
         End If
      End If
      
      rst.Close
      Set rst = Nothing
   
   End If
   
  ' GetImageDB = m_strImageDB
   If (nDBId = 100) Then
      GetImageDB = m_strImageDB
   ElseIf (nDBId = 110) Then
      GetImageDB = m_strImageDBPat
   End If
   
   
End Function


'Public property



