VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Log"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Private colEntries As Collection
Private objDb As WSIDB
Private flgLoging As Boolean
Private m_nEntryCnt As Integer
Private m_bAutoSave As Boolean

Friend Sub SetParent(objDataBase As WSIDB)
    Set objDb = objDataBase
End Sub

Public Sub StartLog(bAutoSave As Boolean)
    m_bAutoSave = bAutoSave
    flgLoging = True
    m_nEntryCnt = 0
    Set colEntries = New Collection
End Sub

Public Sub CancelLog()
    'If Not flgLoging Then Err.Raise 445
    flgLoging = False
    m_nEntryCnt = 0
 
    Set colEntries = Nothing
End Sub

Public Sub WriteLog()
    'If Not flgLoging Then Err.Raise 445
    Dim objEntry As LogEntry
    
    If colEntries Is Nothing Then
    Else
       For Each objEntry In colEntries
           SaveEntry objEntry
       Next objEntry
    End If
    'flgLoging = False
    Set colEntries = Nothing
End Sub

Public Sub AddEntry(objDb As WSIDB, PatientId As Long, BusObj As String, Field As String, _
    ByRef Entry As Variant, strAction As String, Optional Comment As Variant, Optional CycleID As Variant, _
    Optional StimId As Variant)
    
    
    'If Not flgLoging Then Err.Raise 383

    Dim objEntry As New LogEntry
    
' john test
    
    objEntry.PatientId = PatientId
    objEntry.BusObj = BusObj
    objEntry.Field = Field
    
    If IsNull(Entry) Then
        objEntry.Entry = Entry
    Else
        objEntry.Entry = CStr(Entry)
    End If
    
    If IsMissing(Comment) Then
        objEntry.Comment = Null
    Else
        objEntry.Comment = Comment
    End If
    
    If IsMissing(CycleID) Then
        objEntry.CycleID = Null
    Else
        objEntry.CycleID = CycleID
    End If
    
    If IsMissing(StimId) Then
        objEntry.StimId = Null
    Else
        objEntry.StimId = StimId
    End If
    
    objEntry.Userid = objDb.CurrentUser
    objEntry.UserName = objDb.User
    objEntry.Action = strAction
    
    colEntries.Add objEntry
    
    ' autosave
    If m_bAutoSave = True Then
       m_nEntryCnt = m_nEntryCnt + 1
       If m_nEntryCnt > 10 Then
          WriteLog
          StartLog m_bAutoSave
       End If
    End If
    
End Sub
    


Private Sub SaveEntry(objEntry As LogEntry)
        
    Dim cmd As New ADODB.Command
       
    Dim prmPatientId As New ADODB.Parameter
    Dim prmUserID As New ADODB.Parameter
    Dim prmUserName As New ADODB.Parameter
    Dim prmBusObj As New ADODB.Parameter
    Dim prmField As New ADODB.Parameter
    Dim prmEntry As New ADODB.Parameter
    Dim prmComment As New ADODB.Parameter
    Dim prmCycID As New ADODB.Parameter
    Dim prmStimId As New ADODB.Parameter
    Dim prmAction As New ADODB.Parameter
 
    'Set up the parameters for the SP
    
    prmPatientId.Type = adInteger
    prmPatientId.Direction = adParamInput
    prmPatientId.value = objEntry.PatientId
    cmd.Parameters.Append prmPatientId
    
    prmUserID.Type = adVarChar
    prmUserID.Size = 50
    prmUserID.Direction = adParamInput
    prmUserID.value = objDb.CurrentUser
    cmd.Parameters.Append prmUserID
    
    prmUserName.Type = adVarChar
    prmUserName.Size = 50
    prmUserName.Direction = adParamInput
    prmUserName.value = objDb.User
    cmd.Parameters.Append prmUserName
    
    prmBusObj.Type = adVarChar
    prmBusObj.Size = 50
    prmBusObj.Direction = adParamInput
    prmBusObj.value = objEntry.BusObj
    cmd.Parameters.Append prmBusObj
    
    prmField.Type = adVarChar
    prmField.Size = 50
    prmField.Direction = adParamInput
    prmField.value = objEntry.Field
    cmd.Parameters.Append prmField
    
    prmEntry.Type = adLongVarChar
    prmEntry.Size = 2147483647
    prmEntry.Direction = adParamInput
    prmEntry.value = objEntry.Entry
    cmd.Parameters.Append prmEntry
    
    prmComment.Type = adVarChar
    prmComment.Size = 255
    prmComment.Direction = adParamInput
    prmComment.value = objEntry.Comment
    cmd.Parameters.Append prmComment
    
    prmCycID.Type = adInteger
    prmCycID.Direction = adParamInput
    prmCycID.value = objEntry.CycleID
    cmd.Parameters.Append prmCycID

    prmStimId.Type = adInteger
    prmStimId.Direction = adParamInput
    prmStimId.value = objEntry.StimId
    cmd.Parameters.Append prmStimId
    
    prmAction.Type = adVarChar
    prmAction.Size = 32
    prmAction.Direction = adParamInput
    prmAction.value = objEntry.Action
    cmd.Parameters.Append prmAction
    
    'Execute the Stored Proc
    cmd.CommandType = adCmdStoredProc
    cmd.CommandText = "sp_AddLogEntry"
    Set cmd.ActiveConnection = objDb.GetConnection
    cmd.Execute
    
    Set cmd.ActiveConnection = Nothing
    
End Sub

Private Sub Class_Terminate()
   WriteLog
End Sub

