VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LogEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Type LogEntryType
    PatientId As Long
    BusObj As String
    Field As String
    Entry As Variant
    Comment As Variant
    CycleID As Variant
    StimId As Variant
    Userid As Integer
    UserName As String
    Action As String
End Type


Private udtEntry As LogEntryType

Public Property Let PatientId(ByVal vData As Long)
    udtEntry.PatientId = PatientId
End Property
Public Property Get PatientId() As Long
   PatientId = udtEntry.PatientId
End Property
Public Property Let Userid(ByVal vData As Integer)
    udtEntry.Userid = vData
End Property
Public Property Get Userid() As Integer
    Userid = udtEntry.Userid
End Property
Public Property Let UserName(ByVal vData As String)
    udtEntry.UserName = vData
End Property
Public Property Get UserInit() As String
    UserName = udtEntry.UserName
End Property
Public Property Let StimId(ByVal vData As Variant)
    udtEntry.StimId = vData
End Property
Public Property Get StimId() As Variant
    StimId = udtEntry.StimId
End Property

Public Property Let BusObj(ByVal vData As String)
    udtEntry.BusObj = vData
End Property
Public Property Get BusObj() As String
    BusObj = udtEntry.BusObj
End Property

Public Property Let Field(ByVal vData As String)
    udtEntry.Field = vData
End Property
Public Property Get Field() As String
    Field = udtEntry.Field
End Property

Public Property Let Entry(ByVal vData As Variant)
    udtEntry.Entry = vData
End Property
Public Property Get Entry() As Variant
    Entry = udtEntry.Entry
End Property

Public Property Let Comment(ByVal vData As Variant)
    udtEntry.Comment = vData
End Property
Public Property Get Comment() As Variant
    Comment = udtEntry.Comment
End Property

Public Property Let CycleID(ByVal vData As Variant)
    udtEntry.CycleID = vData
End Property
Public Property Get CycleID() As Variant
    CycleID = udtEntry.CycleID
End Property
Public Property Let Action(ByVal vData As String)
    udtEntry.Action = vData
End Property
Public Property Get Action() As String
    Action = udtEntry.Action
End Property


